package org.beetl.core.lab;

public class ChargingStation {
	
	private float[] longitude = new float[]{1.1f};

	public float[] getLongitude() {
		return longitude;
	}

	public void setLongitude(float longitude[]) {
		this.longitude = longitude;
	}
	
	
}
